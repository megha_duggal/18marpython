class circle:
    pi=3.14

    def __init__(self,r):
        #pass
        self.r=r
        print('values initialized')
        
    def __str__(self):                   #string representation or magic function
        #pass
        print('called')
        print(self.pi*self.r*self.r)

    def __del__(self):
        print('del called before',self.r)
        del self.r
        print('values destroyed')
        print(self.r)    

o=circle(10) 
o.__str__()