class person:
    category='human'
    def set_name(self,first,last):
        #return first+last
        self.f=first
        self.l=last

    def print_name(self):
        return "First Name:{} Last Name:{}".format(self.f,self.l,self.category)    

p1=person()
print(type(p1))  
print(p1.category)
print(p1.__class__.category)
print(p1.set_name('peter','parker')) 
print(p1.print_name())   
p1.set_name('Amandeep','kaur')
print(p1.print_name())  

p2=person()
p2.set_name('akshi','sharma')
print(p2.print_name())