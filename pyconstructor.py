class car:
    def __init__(self,color,brand):
        self.c=color 
        self.b=brand             
        #print('It is a class method') # it is a constructor in python

    def print_details(self):   
        print('Color:{} Brand:{}'.format(self.c,self.b))

obj=car('white','bmw')
#print(obj)       #it prints address 
obj.print_details()

obj1=car('black','toyata')
obj1.print_details()

c1=input('enter car color')
br=input('enter car brand')
obj2=car(c1,br)
obj2.print_details()