class details:
    type='student'
    def __init__(self):
        print('parent class')
    def info(self,name,roll_no):
        print('Name:{} Roll_no:{}'.format(name,roll_no))
        

class fee(details):       #Inheritance in python is of 3 types: single,multiple,multilevel
    print('Child Class')
    def total(self,tution,academic):
        return(tution+academic)

obj=fee()    
print(obj.total(1000,2000))
obj.info('aman',1)
print(obj.type)

pobj=details()
pobj.info('peter',100)
#pobj.total(10,10)     this will give an error bcz parent class does not inherit properties of child class
